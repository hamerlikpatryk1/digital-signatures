#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <algorithm>
#include <list>
#include <time.h>

/////////////////////////RABIN
void prime_sieve(int p);
int powerOf2[31] =
        {
                1<<0,  1<<1,  1<<2,  1<<3,  1<<4,  1<<5,  1<<6,
                1<<7,  1<<8,  1<<9,  1<<10, 1<<11, 1<<12, 1<<13,
                1<<14, 1<<15, 1<<16, 1<<17, 1<<18, 1<<19, 1<<20,
                1<<21, 1<<22, 1<<23, 1<<24, 1<<25, 1<<26, 1<<27,
                1<<28, 1<<29, 1<<30
        };

// calculates a^b mod m
int power_modulo_fast(int a, int b, int m)
{
    int i;
    int result = 1;
    long int x = a%m;

    for (i=1; i<=b; i<<=1)
    {
        x %= m;
        if ((b&i) != 0)
        {
            result *= x;
            result %= m;
        }
        x *= x;
    }
    return result;
}

//Miller-Rabin test
int Miller_Rabin(int p, int k)
{
    int s = 0;
    int s2 = 1;
    int a, d, i, r, prime;
    srand(time(NULL));

    if (p<4)
    {
        return 1;
    }
    if (p%2 == 0)
    {
        return 0;
    }

    // calculate s and d
    while ((s2 & (p-1)) == 0)
    {
        s  += 1;
        s2 <<= 1;
    }
    d = p/s2;

    // try k times
    for (i=0; i<k; i++)
    {
        a = 1+(int) ((p-1)*rand()/(RAND_MAX+1.0));
        if (power_modulo_fast(a, d, p) != 1)
        {
            prime = 0;
            for (r=0; r<=s-1; r++)
            {
                if (power_modulo_fast(a, powerOf2[r]*d, p) == p - 1)
                {
                    prime = 1;
                    break;
                }
            }
            if (prime == 0)
            {
                return 0;
            }
        }
    }
    return 1;
}
//////////////////////////FERMAT
struct Dzielnik
{
public:
    int dziel = 0;
    int krotn = 0;
};
typedef Dzielnik Dzielnik;

void wyswietl_wyn(Dzielnik * arr, int l_dziel, int a);
bool is_vowel(char m);

using namespace std;
int pot_mod(int x, int y, int m);
int modInverse(int a, int m);

int main()
{
    //wczytywanie danych z konsoli i kontrola b³êdów
    int p, g, k, r;
    cout<<"Please type p: "<<endl;
    cin >> p;
    cout<<"Podaj dokldnosc sprawdzenia czy jest pierwsza: "<<endl;
    cin >>k;
    while(Miller_Rabin(p, k) != 1 || p>32000)
    {
        cout<<"Please enter p smaller than 32000 and prime number" << endl;
        cin >> p;
    }
    cout<<"Its ok"<<endl;
    /////////////////////////////////obliczanie q

    int  a,d,y;
    int i = 0, l_dziel = 0;
    cout << "Nasze a= " << p-1 <<endl;
    a = p-1;
    d = a;
    while (d % 2 == 0)
    {
        d = d / 2;
        i++;
    }
    cout << a << " =2^" << i << "*" << d << endl;

    int x = floor(sqrt(d));

    Dzielnik arr[4];
    if (i > 0)
    {
        arr[l_dziel].dziel = 2;
        arr[l_dziel].krotn = i;
        l_dziel++;
    }
#pragma region zwykle_D
    if ((double)x == sqrt(d))
    {
        arr[l_dziel].dziel = x;
        arr[l_dziel].krotn = 2;
        l_dziel++;
    }
    else
        x++;
    if ((double)x == sqrt(d) && arr[1].dziel == 1)
    {
        wyswietl_wyn(arr, l_dziel - 1, a);
        return 0;
    }
    int warunek = 0;
    while (x<(d + 1) / 2)
    {
        y = x * x - d;
        if (y>0 && (double)floor(sqrt(y)) == sqrt(y))
        {
            arr[l_dziel].dziel = x + sqrt(y);
            arr[l_dziel].krotn = 1;
            l_dziel++;
            wyswietl_wyn(arr, l_dziel, a);
            break;
        }
        else
            x++;
    }
    int d_prim = (int)(x + sqrt(y));
    int d_bis = (int)(x - sqrt(y));
    x = floor(sqrt(d_prim));
    d = d_prim;
#pragma endregion


#pragma region pochodna_d
    if ((double)x != sqrt(d))
        x++;
    else
    {
        for (i = 0; i<l_dziel - 1; i++)
        {
            if (arr[i].dziel == x)
                break;
            if (arr[i].dziel != x)
                l_dziel++;
            arr[i].dziel = x;
            arr[i].krotn += 2;
        }
    }
    warunek = 0;
    while (x<(d + 1) / 2)
    {
        y = x * x - d;
        if (y>0 && (double)floor(sqrt(y)) == sqrt(y))
        {
            warunek = 1;
            break;
        }
        else
            x++;
    }
    if (warunek == 0)
    {
        for (i = 0; i<l_dziel - 1; i++)
        {
            if (arr[i].dziel == d)
                break;
        }
        if (arr[i].dziel != d)
            l_dziel++;
        arr[i + 1].dziel = d;
        arr[i + 1].krotn += 1;
    }
    else
    {
        d = x + (int)sqrt(y);
        for (i = 0; i<l_dziel - 1; i++)
            if (arr[i].dziel == d)
                break;
        if (arr[i].dziel != d)
            l_dziel++;
        arr[i + 1].dziel = d;
        arr[i + 1].krotn += 1;
        d = x - sqrt(y);
        for (i = 0; i<l_dziel - 1; i++)
        {
            if (arr[i].dziel == d)
                break;
        }
        if (arr[i].dziel != d)
            l_dziel++;
        arr[i + 1].dziel = d;
        arr[i + 1].krotn += 1;
    }
    d = d_bis;
    x = floor(sqrt(d_bis));
#pragma endregion

#pragma region druga_pochodna_d
    if ((double)x != sqrt(d))
        x++;
    warunek = 0;

    while (x<(d + 1) / 2)
    {
        y = x * x - d;
        if (y > 0 && (double)floor(sqrt(y)) == sqrt(y))
        {
            warunek = 1;
            break;
        }
        else
            x++;
    }
    if (warunek == 0)
    {
        for (i = 0; i<l_dziel - 1; i++)
        {
            if (arr[i].dziel == d)
                break;
        }
        if (arr[i].dziel != d)
            l_dziel++;
        arr[i + 1].dziel = d;
        arr[i + 1].krotn += 1;
    }
    else
    {
        d = x + (int)sqrt(y);
        for (i = 0; i < l_dziel - 1; i++)
        {
            if (arr[i].dziel == d)
                break;
        }
        if (arr[i].dziel != d)
            l_dziel++;
        arr[i + 1].dziel = d;
        arr[i + 1].krotn += 1;
        d = x - sqrt(y);
        for (i = 0; i<l_dziel - 1; i++)
        {
            if (arr[i].dziel == d)
                break;
        }
        if (arr[i].dziel != d)
            l_dziel++;
        arr[i + 1].dziel = d;
        arr[i + 1].krotn += 1;
    }

#pragma endregion


    wyswietl_wyn(arr, l_dziel, a);
    int q;
    cout<<"Wpisz najwiekszy dzielnik (nie mam juz sily na petle i nowe tablice)"<<endl;
    cin>> q;
    cout<<"To nasze q: "<< q<<endl; //wpisz 7919

    cout<<"Wpisz liczbe naturalna g"<<endl;
    cin>>g;
    // while(pow(g,q) != 1 % p)
    //    {
    //        cout<<"Wpisz liczbe spelniajaca rownanie g^q = 1 mod p" << endl;
    //        cin >> g;
    //    }

    cout<<"Wpisz k"<<endl;
    cin>>k;
    while(k>q)
    {
        cout<<"Wpisz k jest mniejsze od q" << endl;
        cin >> k;
    }
    int g_k;

    g_k = pow(g,k);
    g_k %= p;
    g_k+=1; //klucz za ka¿dym razem by³ o 1 za ma³y
    cout<<"Klucz publiczny G^k: "<<g_k<<endl;
    cout<<"Publiczny rejestr klucza (PRK): "<<"("<<g_k<<","<<g<<","<<p<<","<<q<<")"<<endl;

    cout<<"Wpisz liczbe r"<<endl;
    cin>>r;
    while(r>q)
    {
        cout<<"Wpisz r jest mniejsze od q" << endl;
        cin >> r;
    }
    ////////wczytanie tekstu m
    string m;
    fstream file;
    file.open("tekst.txt"); //wczyta z pliku txt
    if (file.fail())
    {
        cerr << "Error opening file" << endl;
    }
    while (m != "");
    {
        getline(file, m);
    }

    file.close();

    ///////Obliczanie JHA

    cout << "Text from file: " << m << endl;

    int number_v = 0;
    int number_c = 0;
    int number_s = 0; //spaces


    for (int i = 0; i <= m.length(); i++)
        if (isalpha(m[i]))
            if (is_vowel(m[i]))
                number_v++;

    for (int i = 0; i <= m.length(); i++)
        if (isalpha(m[i]))
            if (!is_vowel(m[i]))
                number_c++;

    for (int i = 0; i <= m.length(); i++)
        if (m[i] == ' ')
            number_s++;


    cout << "Number of vowel: " << number_v << endl;
    cout << "Number of conso: " << number_c << endl;
    cout << "Number of spaces: " << number_s << endl;

    //JHA

    long long int jha,power;
    power=(7*number_v)-(3*number_c)+(number_s*number_s);
    //if

    cout<<"JHA: "<<pot_mod(q, power, p)<<endl;

    //cout<<"Power: "<<power<<endl;
    // cout<<"Q: "<<q<<endl;
    // jha= (pow(q,power));
    //  cout <<"1.JHA before modulo: "<<jha<<endl;
    //  jha %= p;
    //  cout <<"2.JHA after modulo: "<<jha<<endl;

    // jha = pow(q,power);
    // jha %= p;
    //  cout <<"2.JHA after modulo: "<<jha<<endl;

    //  cout<<"JHA = "<<q<<"^"<<"7*"<<number_v<<"-3*"<<number_c<<"+("<<number_s<<"^2)"<<"mod "<<p<<endl;
//   cout<<"JHA: "<<jha<<endl;
    //else if (power =< 0) //można dokończyć dla elementu ujemnego

    // cout<<"CHECKING 5^57 = "<<pow(5,57);

    //// II Etap
    int x_x,y_y;
    x_x = g_k % q;

    jha = pot_mod(q, power, p);
    int res_y = r *(jha+k*x_x);
    cout << "Rest y = "<<res_y << "It should be 1508"<<endl;
    y_y = res_y % q;
    //y_y = modInverse(res_y, q);
    //  cout<< "y= " <<modInverse(res_y, q)<<endl;
    //y_y = pow(r,-1)*(m+k*x_x); //////////////do dokoñczenia zapytaæ siê czym jest ten "m"

    cout<<"Podpis wiadomosci: s("<<x_x<<","<<y_y<<")"<<endl;


}

void wyswietl_wyn(Dzielnik * arr, int l_dziel, int a)
{
    cout << endl;
    cout << "Liczba a posiada dzielniki: (";
    for (int i = 0; i<l_dziel; i++)
    {
        cout << arr[i].dziel;
        if (i != l_dziel - 1)
            cout << ",";
    }
    cout << ")" << endl;
    cout << "O krotnosciach: (";
    for (int i = 0; i<l_dziel; i++)
    {
        cout << arr[i].krotn;
        if (i != l_dziel - 1)
            cout << ",";
    }
    cout << ")";
}
bool is_vowel(char m)
{
    char letter = tolower(m);
    if (letter == 'a'
        || letter == 'e'
        || letter == 'i'
        || letter == 'o'
        || letter == 'u')
        return true;
    return false;
}
int pot_mod(int x, int y, int m)
{
    int w = x;
    for(int i =1; i<y; i++)
    {
        w=(w*x) % m;
    }
    return w;
}
int modInverse(int a, int m)
{
    a = a%m;
    for (int x=1; x<m; x++)
        if ((a*x) % m == 1)
            return x;
}